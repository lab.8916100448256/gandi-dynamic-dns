# Gandi Dynamic DNS

Cron job to auto update DNS records when WAN IP changes using Gandi Live DNS API on Debian GNU/Linux

(This is obviously for a domain hosted by [Gandi.net](https://www.gandi.net/))

## Requirements
+ Debian GNU/Linux 10 (Buster)
+ curl : `apt-get install curl`
+ dig : `apt-get install dnsutils`

## Install 
Fetch script from git repo, install and configure
```
git clone https://gitlab.com/lab.8916100448256/gandi-dynamic-dns.git ./gandi-dynamic-dns
cd ./gandi-dynamic-dns
sudo bash ./install.sh
```

Then configure DNS record info and API key in file /etc/live-dns/live-dns.conf

```
nano /etc/live-dns/live-dns.conf
```


## How it works

### /etc/cron.d/live-dns-update
Cron job definition to check for WAN IP change and call Live DNS API to update the DNS record if necessary. 

By default runs the check/update script 2 times per hour, at every hour past 12 and 42 minutes.

### /etc/live-dns/live-dns.conf
Configuration file for the check/update script.

Needs to contains 3 environment variable definitions
+ DOMAIN="domain.tld" : Replace domain.tld with your domain name
+ RECORD="host" : Replace host with the sub domain you want to update when WAN IP changes
+ APIKEY="Your-API-Key" : Replace with you API key generated in security section of your profile on Gandi web interface.

Check this page if you don't know the APIKey of your Gandi account : https://docs.gandi.net/en/domain_names/advanced_users/api.html

### /usr/local/bin/live-dns-update.sh
This is the script that is called by the cron job.

+ First the WAN (external) IPV4 and IPV6 adresses are determined using http://me.gandi.net through curl
+ Then they are compared to the A and AAAA DNS records retrieved through dig for the configured host and domain.
+ In case of mismatch Gandi live DNS API is called to update the DNS records through curl using the configured APIKey

Information logs are written to the system log `/var/log/syslog`. 

For Example : 

```
December 12 12:42:02 passerelle root: WAN IPv4 : 1.2.3.4
December 12 12:42:02 passerelle root: WAN IPv6 : 1111:2222:3333:4444:5555:6666:7777:8888
December 12 12:42:02 passerelle root: DNS IPv4 : 1.2.3.4
December 12 12:42:02 passerelle root: DNS IPv6 : 1111:2222:3333:4444:5555:6666:7777:8888
December 12 12:42:02 passerelle root: No need to update IPv4 DNS record for sub.domain.tld
December 12 12:42:02 passerelle root: No need to update IPv6 DNS record for sub.domain.tld
```

This script is based on the [mywanip.sh](https://github.com/Gandi/api-examples/blob/master/bash/livedns/mywanip.sh) sample provided by Gandi on their GitHub

