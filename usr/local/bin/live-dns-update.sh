#!/bin/bash
#
# Updates DNS zone records using Gandi's LiveDNS API.
# Before you use this script you need to configure record details and API key in 
# the file `/etc/live-dns/live-dns.conf` 
#
# This script is meant to be run by cron.
# through a crontab placed in /etc/cron.d/live-dns-update
#
# This script is based on the sample provided here : 
# https://github.com/Gandi/api-examples/blob/master/bash/livedns/mywanip.sh
#

# Include configuration file
source /etc/live-dns/live-dns.conf
#echo Domain : $DOMAIN
#echo Record : $RECORD
#echo APIKey : $APIKEY

API="https://dns.api.gandi.net/api/v5/"
IP_SERVICE="http://me.gandi.net"


# Get WAN IP as seend from outside
WAN_IP4=$(curl -s4 $IP_SERVICE)
WAN_IP6=$(curl -s6 $IP_SERVICE)
echo WAN IPv4 : $WAN_IP4 | logger
echo WAN IPv6 : $WAN_IP6 | logger

if [[ -z "$WAN_IP4" && -z "$WAN_IP6" ]]; then
    echo "Something went wrong. Can not get your IP from $IP_SERVICE " | logger
    exit 1
fi

# Get IP from DNS
DNS_IP4=$(dig +short @ns1.gandi.net -t A $RECORD.$DOMAIN)
DNS_IP6=$(dig +short @ns1.gandi.net -t AAAA $RECORD.$DOMAIN)

echo DNS IPv4 : $DNS_IP4 | logger
echo DNS IPv6 : $DNS_IP6 | logger

# Update IPV4 DNS record if IP adresses mismatch
if [[ ! -z "$WAN_IP4" ]]; then
  if [[ "$WAN_IP4" != "$DNS_IP4" ]]
  then
    echo Updating IPv4 DNS records for $RECORD.$DOMAIN | logger
    DATA='{"rrset_values": ["'$WAN_IP4'"]}'
    IP4_RES=$(curl -s -XPUT -d "$DATA" \
        -H"X-Api-Key: $APIKEY" \
        -H"Content-Type: application/json" \
        "$API/domains/$DOMAIN/records/$RECORD/A" 2>&1)
    echo Update of IPv4 DNS record : $IP4_RES | logger
  else
    echo No need to update IPv4 DNS record for $RECORD.$DOMAIN | logger
  fi
fi


# Update IPV6 DNS record if IP adresses mismatch
if [[ ! -z "$WAN_IP6" ]]; then
  if [[ "$WAN_IP6" != "$DNS_IP6" ]]
  then
    echo Updating IPv6 DNS records for $RECORD.$DOMAIN | logger
    DATA='{"rrset_values": ["'$WAN_IP6'"]}'
    IP6_RES=$(curl -s -XPUT -d "$DATA" \
        -H"X-Api-Key: $APIKEY" \
        -H"Content-Type: application/json" \
        "$API/domains/$DOMAIN/records/$RECORD/AAAA" 2>&1)
    echo Update of IPv6 DNS record : $IP6_RES | logger
  else
    echo No need to update IPv6 DNS record for $RECORD.$DOMAIN | logger
  fi
fi
